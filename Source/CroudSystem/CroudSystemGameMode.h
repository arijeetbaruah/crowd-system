// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CroudSystemGameMode.generated.h"

UCLASS(minimalapi)
class ACroudSystemGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACroudSystemGameMode();
};



