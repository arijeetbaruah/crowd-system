// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC_AIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Kismet/KismetMathLibrary.h"
#include "AICharacter.h"

void ANPC_AIController::OnPossess(APawn* const InPawn)
{
	Super::OnPossess(InPawn);

	if (AAICharacter* const cont = Cast<AAICharacter>(InPawn))
	{
		aiCharacter = cont;
		if (UBehaviorTree* const tree = cont->GetBehaviorTree())
		{
			UBlackboardComponent* b;
			UseBlackboard(tree->BlackboardAsset, b);
			Blackboard = b;
			RunBehaviorTree(tree);
		}
	}
}

void ANPC_AIController::Tick(float const DeltaTime)
{
	Super::Tick(DeltaTime);

	if (aiCharacter)
	{
		hunger = FMath::Min(aiCharacter->maxHunger, hunger + aiCharacter->hungerIncreaseRate * DeltaTime);
		bool _isHungry = aiCharacter->hungerValidValue < hunger;
		GetBlackboardComponent()->SetValueAsBool("isHunger", _isHungry);
	}
}

float ANPC_AIController::GetHunger() const
{
	return hunger;
}

void ANPC_AIController::SetHunger(float const newHunger)
{
	hunger = newHunger;
	GetBlackboardComponent()->SetValueAsFloat("isHunger", IsHungry());
}

bool ANPC_AIController::IsHungry()
{
	return aiCharacter->hungerValidValue < GetHunger();
}
