// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_Eat.h"
#include "AIController.h"
#include "NPC_AIController.h"

UBTT_Eat::UBTT_Eat(FObjectInitializer const& ObjectInitializer)
{
	NodeName = TEXT("Eat");
}

EBTNodeResult::Type UBTT_Eat::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (AAIController* const cont = OwnerComp.GetAIOwner())
	{
		if (ANPC_AIController* const npc = Cast<ANPC_AIController>(cont))
		{
			npc->SetHunger(0);
			
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
