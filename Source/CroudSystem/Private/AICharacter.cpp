// Fill out your copyright notice in the Description page of Project Settings.


#include "AICharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "NPC_AIController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"

// Sets default values
AAICharacter::AAICharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	perceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception"));

	sightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	sightConfig->SightRadius = sightRadius;
	sightConfig->LoseSightRadius = sightRadius + loseSightRadius;
	sightConfig->PeripheralVisionAngleDegrees = 360.0f;
	sightConfig->DetectionByAffiliation.bDetectEnemies = true;
	sightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	sightConfig->DetectionByAffiliation.bDetectFriendlies = true;

	perceptionComponent->ConfigureSense(*sightConfig);
	perceptionComponent->SetDominantSense(sightConfig->GetSenseImplementation());
	perceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AAICharacter::OnTargetPerceptionInfoUpdated);
}

// Called when the game starts or when spawned
void AAICharacter::BeginPlay()
{
	Super::BeginPlay();

}

void AAICharacter::OnTargetPerceptionInfoUpdated(const TArray<AActor*>& updatedActors)
{
	UE_LOG(LogTemp, Warning, TEXT("OnTargetPerceptionInfoUpdated"));

	for (int index = 0; index < updatedActors.Num(); index++)
	{
		FVector location = updatedActors[index]->GetActorLocation();
		UAIBlueprintHelperLibrary::GetBlackboard(this)->SetValueAsVector("TargetLocation", location);
	}
}

// Called every frame
void AAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ANPC_AIController* aiController = GetController<ANPC_AIController>();
	float hunger = aiController->GetHunger();
	hunger += hungerIncreaseRate * DeltaTime;
	aiController->SetHunger(hunger);
}

// Called to bind functionality to input
void AAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UBehaviorTree* AAICharacter::GetBehaviorTree() const
{
	return BehaviorTree;
}

