// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_IsHungry.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "NPC_AIController.h"

EBTNodeResult::Type UBTT_IsHungry::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (ANPC_AIController* cont = Cast<ANPC_AIController>(OwnerComp.GetAIOwner()))
	{
		
		if (cont->IsHungry())
		{
			OwnerComp.GetBlackboardComponent()->SetValueAsBool(GetSelectedBlackboardKey(), true);
			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
