// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTT_Eat.generated.h"

/**
 * 
 */
UCLASS()
class CROUDSYSTEM_API UBTT_Eat : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	explicit UBTT_Eat(FObjectInitializer const& ObjectInitializer);
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
