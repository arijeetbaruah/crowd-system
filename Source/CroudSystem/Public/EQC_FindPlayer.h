// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "EQC_FindPlayer.generated.h"

/**
 * 
 */
UCLASS()
class CROUDSYSTEM_API UEQC_FindPlayer : public UEnvQueryContext
{
	GENERATED_BODY()
public:
	void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const;

};
