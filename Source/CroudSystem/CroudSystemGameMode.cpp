// Copyright Epic Games, Inc. All Rights Reserved.

#include "CroudSystemGameMode.h"
#include "CroudSystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACroudSystemGameMode::ACroudSystemGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
