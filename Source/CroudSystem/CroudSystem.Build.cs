// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CroudSystem : ModuleRules
{
    public CroudSystem(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "EnhancedInput",
            "AIModule",
            "GameplayTasks",
            "NavigationSystem"
        });
    }
}
